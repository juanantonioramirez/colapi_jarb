var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var userFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapi_db/collections/';
var apiKeyMLab = 'apiKey=1CUxSF-NTaIA9K3I_AcIcIKM1FxwG6tP';

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto" + port + "..." );



//LOGOUT mLab
app.post(URLbase + 'logoutmlab',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var constasenaIng = req.body.password;
    var correo = req.body.email;
    var usuario = req.body.id;
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString1 = 'q={"id":';
    var queryString2 ='}&';

    clienteMlab.get('user?' + queryString1 + usuario + queryString2 + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
      console.log(bodymLab);
      var consulta = {};
      consulta = bodymLab[0];
      console.log(consulta[0]);
      console.log(consulta);
      var contrasenaCon = consulta.password;

      var nuevoDes = {
        "logged": true
                     }
      console.log(nuevoDes);
      var cambio = '{"$unset":' + JSON.stringify(nuevoDes) + '}';
      console.log(cambio);

      clienteMlab.put('user?'+ queryString1 + usuario + queryString2 + apiKeyMLab, JSON.parse(cambio),
          function(errMLab2, respuestaMLab2, bodymLab2) {
          console.log('Error2 : ' + errMLab2);
          console.log('Respuesta MLab2 : ' + respuestaMLab2);
          console.log('Body2 : ' + bodymLab2);
          res.send({"msg" : "Logout correcto.", "idUsuario" : consulta.email});
        });


     });
  });


//LOGIN mLab
app.post(URLbase + 'loginmlab',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var constasenaIng = req.body.password;
    var correo = req.body.email;
    var usuario = req.body.id;
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString1 = 'q={"id":';
    var queryString2 ='}&';

    clienteMlab.get('user?' + queryString1 + usuario + queryString2 + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log(bodymLab);
      var consulta = {};
      consulta = bodymLab[0];
      console.log(consulta[0]);
      console.log(consulta);
      var contrasenaCon = consulta.password;

      if(constasenaIng == contrasenaCon) {
        consulta.logged = true;
        console.log(consulta);
        console.log("Login correcto");

        var cambio = '{"$set":' + JSON.stringify(consulta) + '}';
        console.log(cambio);
        console.log(JSON.parse(cambio));

        clienteMlab.put('user?'+ queryString1 + usuario + queryString2 + apiKeyMLab, JSON.parse(cambio),
          function(errMLab2, respuestaMLab2, bodymLab2) {
          console.log('Error2 : ' + errMLab2);
          console.log('Respuesta MLab2 : ' + respuestaMLab2);
          console.log('Body2 : ' + bodymLab2);
          res.send({"msg" : "Login correcto.", "idUsuario" : consulta.email});
        });

       } else {
        res.send({"msg" : "Contraseña incorrecta.", "idUsuario" : consulta.email});
       }

    });
  });


  //GET con id MLab
  app.get(URLbase + 'user/:id',
   function (req, res) {
     console.log("GET /colapi/v3/user/:id");
     console.log(req.params.id);
     var id = req.params.id;
     var queryString = 'q={"id":'+ id +'}&f={"_id":0}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apiKeyMLab,
       function(errMLab, respuestaMLab, bodymLab) {
       console.log('Error : ' + errMLab);
       var respuestaBody = {};
   // Se valida si el parametro error es verdadero o falso para asignar a respuesta el body
   //    errMLab = 400;
       respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar users de mLab"};
       console.log(respuestaBody[0]);
       res.send(respuestaBody[0])  ;
     });

  });

  //GET General
  app.get(URLbase + 'users',
   function (req, res) {
     console.log("GET /colapi/v3/users");
     console.log(req.params.id);
     var id = req.params.id;
     var queryString = 'f={"_id":0}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apiKeyMLab,
       function(errMLab, respuestaMLab, bodymLab) {
       console.log('Error : ' + errMLab);
       var respuestaBody = {};
   // Se valida si el parametro error es verdadero o falso para asignar a respuesta el body
   //    errMLab = 400;
       respuestaBody = !errMLab ? bodymLab : {"msg" : "Error al recuperar users de mLab"};
  //     console.log(respuestaBody[0]);
       res.send(respuestaBody)  ;
     });

  });






//POST mLab, crear un cliente
app.post(URLbase + 'users',
  function(req, res) {
    console.log("PUT/colapi/V3");
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString = 'f={"_id":0, "first_name":0, "last_name":0, "email":0, "password":0}&s={"id":-1}&l=1&';
   //
    clienteMlab.get('user?' + queryString + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log(bodymLab);
      var idRecibe = {};
      idRecibe = bodymLab[0];
      var numero = idRecibe.id;
      var numero2 = parseInt(numero) + 1;
      console.log(idRecibe.id);
      console.log(numero2);
      var nuevoUsuario = {
         "id" : numero2,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
      };
    //
    clienteMlab.post('user?'+ apiKeyMLab, nuevoUsuario,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log('Error : ' + errMLab);
      console.log('Respuesta MLab : ' + respuestaMLab);
      console.log('Body : ' + bodymLab);
      res.send(nuevoUsuario);
   });
  });
});
