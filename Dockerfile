#Imagen inicial a partir de la cual creamos nuestra imagen
FROM node

#Definimos directorio del contenedor
WORKDIR /colapi_jarb

#Añadmos contenido del proyecto en directorio de contenedor
ADD . /colapi_jarb

#Puerto escucha el contenedor (el mismo que se definió en colapi)
EXPOSE 3000

#Comandos para lanzar nuestra API REDT colapi
CMD ["npm","start"]
